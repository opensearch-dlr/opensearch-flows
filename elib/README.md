# Download elib jsons
Download meta information for each publication via provided json export endpoint. For each publication, a single json-file will be created.

```mermaid
flowchart TD
    A(Create destination directory if necessary) --> B(Get latest elib ID from website) 
    --> C(Get latest elib ID from destination folder) --> D(Get elib ID range to download)
    -- Map --> E(Download elib json to destination) --> F(Create report)
    E(Download elib json to destination)  --> G[(elib json files)]
```

# elib annotator
Fetches elib data from an arango database and create embedding for the title and the abstract. Uses https://github.com/DLR-SC/corpus-annotation-graph-builder

# elib cag
Read json files which are downloaded by download_elib_jsons.py and creates an Graph. Uses https://github.com/DLR-SC/corpus-annotation-graph-builder

# Create elib documents in elastic
```mermaid
flowchart TD
    A(Get latest elib ID from destination folder) --> B(Get highest elib ID from elastic) 
    --> C(Get diff of IDs) 
    -- Map --> D(Create elastic doc from json file) --> G[(elastic index)]
```