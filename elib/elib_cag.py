import glob
import json
import os
import re

import prefect
import redis
from cag.framework import GraphCreatorBase
from cag.graph_elements.nodes import GenericOOSNode, Field
from cag.graph_elements.relations import GenericEdge
from cag.utils import utils
from cag.utils.config import Config
from cag.view_wrapper.arango_analyzer import ArangoAnalyzer, AnalyzerList
from cag.view_wrapper.link import Link, Field as ViewField
from cag.view_wrapper.view import View
from prefect import task, Flow
from prefect.storage import GitLab

# The redis credentials must be available as environment variables
r = redis.Redis(
    host=str(os.environ.get("REDIS_HOST")),
    port=int(os.environ.get("REDIS_PORT")),
    db=0,
    decode_responses=True,
)
ELIB_CORPUS_DIR = r.get("ELIB_CORPUS_DIR")
assert ELIB_CORPUS_DIR is not None

ARANGO_HOST = r.get("ARANGO_HOST")
assert ARANGO_HOST is not None

ARANGO_USER = r.get("ARANGO_USER")
assert ARANGO_USER is not None

ARANGO_PASSWORD = r.get("ARANGO_PASSWORD")
assert ARANGO_PASSWORD is not None

ARANGO_CAG_DB = r.get("ARANGO_CAG_DB")
assert ARANGO_CAG_DB is not None

ARANGO_GRAPH = r.get("ARANGO_GRAPH")
assert ARANGO_GRAPH is not None

ALREADY_PROCESSED = r.get("ELIB_CAG_ALREADY_PROCESSED")
assert ALREADY_PROCESSED is not None


class Files(GenericOOSNode):
    _name = "Method"
    _fields = {"name": Field(), **GenericOOSNode._fields}


class Method(GenericOOSNode):
    _name = "Method"
    _fields = {"name": Field(), **GenericOOSNode._fields}


class Task(GenericOOSNode):
    _name = "Task"
    _fields = {"name": Field(), **GenericOOSNode._fields}


class Publication(GenericOOSNode):
    _name = "Publication"
    _fields = {
        "display_name": Field(),
        "eprintid": Field(),
        "official_url": Field(),
        "title": Field(),
        "year": Field(),
        "doi": Field(),
        "isbn": Field(),
        "publisher": Field(),
        "geo": Field(),
        "geo_mean": Field(),
        **GenericOOSNode._fields,
    }


class Person(GenericOOSNode):
    _name = "Person"
    _fields = {
        "display_name": Field(),
        "first_name": Field(),
        "last_name": Field(),
        "email": Field(),
        "orcid_id": Field(),
        **GenericOOSNode._fields,
    }


class Institution(GenericOOSNode):
    _name = "Institution"
    _fields = {"display_name": Field(), "name": Field(), **GenericOOSNode._fields}


class Category(GenericOOSNode):
    _name = "Category"
    _fields = {"name": Field(), **GenericOOSNode._fields}


class Abstract(GenericOOSNode):
    _fields = {
        "text": Field(),
        **GenericOOSNode._fields
    }


class CorpusHas(GenericEdge):
    _fields = GenericEdge._fields


class HasPerson(GenericEdge):
    _fields = GenericEdge._fields


class PubHasAbstract(GenericEdge):
    _fields = GenericEdge._fields


class RepoHasText(GenericEdge):
    _fields = GenericEdge._fields


class HasCategory(GenericEdge):
    _fields = GenericEdge._fields


class HasKeyterm(GenericEdge):
    _fields = GenericEdge._fields


class HasData(GenericEdge):
    _fields = GenericEdge._fields


class PubHasRepo(GenericEdge):
    _fields = GenericEdge._fields


class PubHasMethod(GenericEdge):
    _fields = GenericEdge._fields


class ELibGraphCreator(GraphCreatorBase):
    _name = "Elib"
    _description = "Graph based on the DLR elib corpus"

    CorpusHas = "CorpusHas"
    HasPerson = "HasPerson"
    PubHasAbstract = "PubHasAbstract"
    HasCategory = "HasCategory"
    HasKeyterm = "HasKeyterm"
    HasData = "HasData"
    Abstract = "Abstract"
    _edge_definitions = [
        {
            'relation': CorpusHas,
            'from_collections': [GraphCreatorBase._CORPUS_NODE_NAME, ],
            'to_collections': [Publication._name]
        },
        {
            'relation': HasPerson,
            'from_collections': [Publication._name],
            'to_collections': [Person._name]
        },
        {
            'relation': PubHasAbstract,
            'from_collections': [Publication._name],
            'to_collections': [Abstract]
        },
        {
            'relation': HasKeyterm,
            'from_collections': [Publication._name, GraphCreatorBase._TEXT_NODE_NAME,
                                 GraphCreatorBase._KEY_TERM_NODE_NAME],
            'to_collections': [GraphCreatorBase._KEY_TERM_NODE_NAME]
        }
    ]

    domain_institution_dict = {"dlr.de": "DLR", "google.com": "Google"}

    def init_graph(self):
        if ALREADY_PROCESSED:
            already_processed = set(json.loads(str(ALREADY_PROCESSED)))
        else:
            already_processed = set()
        node_corpus = self.create_corpus_node(
            key="Elib",
            name=ELibGraphCreator._name,
            type="digital_library",
            desc=ELibGraphCreator._description,
            created_on=self.now,
            timestamp=self.now,
        )
        for pub in self.corpus_file_or_dir:
            with open(pub, "r") as f:
                publication_data = json.load(f)

                selected_attributes = [
                    "keywords",
                    "year",
                    "publisher",
                    "doi",
                    "isbn",
                    "abstract",
                    "title",
                    "official_url",
                    "authors",
                    "institution",
                    "predicted",
                    "issn",
                ]
                attributes: dict = self.get_attributes(
                    publication_data, selected_attributes
                )
                pub_id = f"elib_{str(publication_data['eprintid'])}"
                publication = {
                    "_key": pub_id,
                    "eprintid": str(publication_data["eprintid"]),
                    "year": attributes["year"],
                    "publisher": attributes["publisher"],
                    "doi": attributes["doi"],
                    "isbn": attributes["isbn"],
                    "abstract": attributes["abstract"],
                    "title": attributes["title"],
                    "official_url": attributes["official_url"],
                    "display_name": attributes["title"],
                    "issn": attributes["issn"],
                }
                pub_vert = self.upsert_node(Publication._name, publication)
                self.upsert_edge(
                    ELibGraphCreator.CorpusHas, node_corpus, pub_vert
                )
                for kw in attributes["keywords"]:
                    kw_vert = self.upsert_node(
                        ELibGraphCreator._KEY_TERM_NODE_NAME, kw, alt_key="name"
                    )
                    if kw_vert is not None:
                        self.upsert_edge(
                            ELibGraphCreator.HasKeyterm, pub_vert, kw_vert
                        )

                for auth in attributes["authors"]:
                    auth_vert = self.upsert_node(
                        Person._name, auth
                    )
                    if auth_vert is not None:
                        self.upsert_edge(
                            ELibGraphCreator.HasPerson, pub_vert, auth_vert
                        )
                if attributes["abstract"] is not None:
                    text_key = f"text_{pub_id}"
                    text_data = {"text": attributes["abstract"], "_key": text_key}
                    abstract_vert = self.upsert_node(
                        ELibGraphCreator.Abstract, text_data
                    )
                    self.upsert_edge(
                        ELibGraphCreator.PubHasAbstract,
                        pub_vert,
                        abstract_vert,
                        {},
                    )
            already_processed.add(pub)
        r.set("ELIB_CAG_ALREADY_PROCESSED", json.dumps(list(already_processed)))

    def create_view(self):
        """
        This method creates Views required for searches from the frontend
        It should be only called (manually) when the View plus Analysers aren't created yet!
        TODO: Automate this task
        """
        analyzer_ngram = ArangoAnalyzer("fuzzy_search_bigram")
        analyzer_ngram.set_edge_ngrams(max=3)
        analyzer_ngram.type = ArangoAnalyzer._TYPE_NGRAM
        analyzer_ngram.set_features()

        analyzer_ngram.create(self.arango_db)

        analyzer_token = ArangoAnalyzer("en_tokenizer")
        analyzer_token.set_stopwords(include_default=False)
        analyzer_token.stemming = True
        analyzer_token.accent = False
        analyzer_token.type = ArangoAnalyzer._TYPE_TEXT
        analyzer_token.set_features()

        analyzer_token.create(self.arango_db)

        # Create Link - a view can hvae 0 to * links
        link = Link(name="Publication")  # Name of a collection in the database
        linkAnalyzers = AnalyzerList(
            ["fuzzy_search_bigram", "en_tokenizer"])
        link.analyzers = linkAnalyzers

        # A link can have 0..* fields
        # text_en is a predifined analyzer from arango
        title_field = ViewField("title", AnalyzerList(
            ["fuzzy_search_bigram", "en_tokenizer"]))
        abstract_field = ViewField("abstract", AnalyzerList(
            ["fuzzy_search_bigram", "en_tokenizer"]))
        title_emb_field = ViewField("title_emb")
        abstract_emb_field = ViewField("abstract_emb")
        id_field = ViewField("_id")
        display_field = ViewField("display_name", AnalyzerList(
            ["fuzzy_search_bigram", "en_tokenizer"]))
        #
        # "title_emb": {},
        # "title": {},
        # "display_name": {},
        # "abstract_emb": {},
        # "abstract": {},
        # "_id": {}
        link.add_field(title_field)
        link.add_field(abstract_field)
        link.add_field(title_emb_field)
        link.add_field(abstract_emb_field)
        link.add_field(id_field)
        link.add_field(display_field)
        # create view
        view = View('publications_view',
                    view_type="arangosearch")
        # add the link (can have 0 or 1 link)
        view.add_link(link)

        # can have 0..* primary sort
        view.add_primary_sort("title", asc=False)
        view.add_stored_value(["title"], compression="lz4")
        try:
            view.create(self.arango_db)
        except Exception as e:
            print("Error creating view, please delete the one on DB?", e)
            raise e

    def get_attributes(self, datapoint: dict, selected_attributes):
        res = {}
        def conv(x):
            return x or ""

        for attribute in selected_attributes:
            if attribute == "keywords":
                keywords = []
                if "keywords" in datapoint:
                    keywords_str: str = datapoint["keywords"]
                    chars = [",", ";", "\n", "\r\n"]
                    tmp = [(char, keywords_str.count(char)) for char in chars]
                    split_char = max(tmp, key=lambda x: x[1])[0]
                    keywords = [
                        keyword.strip()
                        for keyword in keywords_str.split(split_char)
                        if len(keyword.strip()) > 0
                    ]
                    keywords = [
                        {"name": keyword, "_key": utils.encode_name(keyword)}
                        for keyword in keywords
                    ]
                res.update({"keywords": keywords})
            elif attribute == "year":
                if type(datapoint["date"]) == int:
                    year = datapoint["date"]
                else:
                    year = int(datapoint["date"].split("-")[0])
                res.update({"year": year})
            elif attribute == "authors":
                authors = []
                if "creators" in datapoint:
                    for creator in datapoint["creators"]:
                        if "id" in creator:
                            creator_id = str(creator["id"])

                            email = ""
                            person_institution_name = ""
                            orcid_id = ""

                            if re.match(r"[^@ \t\r\n]+@.+\.[A-z]+", creator_id):
                                email = creator_id.lower()
                                domain = re.search(r"(?<=@).+\.[A-z]+", email).group()
                                if domain.lower() in self.domain_institution_dict:
                                    person_institution_name = (
                                        self.domain_institution_dict[domain.lower()]
                                    )
                            elif creator_id != "":
                                person_institution_name = creator_id

                            if "orcid" in creator:
                                orcid_id = conv(creator["orcid"])
                            first_name = conv(creator["name"]["given"])
                            last_name = conv(creator["name"]["family"])
                            author_tmp = {
                                "_key": utils.encode_name(f"{first_name} {last_name}"),
                                "display_name": f"{first_name} {last_name}",
                                "org": person_institution_name,
                                "email": email,
                                "orcid_id": orcid_id,
                                "first_name": first_name,
                                "last_name": last_name,
                            }
                            authors.append(author_tmp)
                res.update({"authors": authors})
            else:
                if attribute in datapoint:
                    res.update({attribute: datapoint[attribute]})
                else:
                    res.update({attribute: None})
        return res


@task(name="Get corpus files to process")
def get_delta():
    paths = set()
    if ALREADY_PROCESSED:
        already_processed = set(json.loads(str(ALREADY_PROCESSED)))
    else:
        already_processed = set()

    for filename in glob.iglob(
            f"{ELIB_CORPUS_DIR}{os.path.sep}*.json*", recursive=True
    ):
        paths.add(os.path.abspath(filename))
    delta = paths - already_processed
    return delta


@task(name="Run cag")
def update_graph(files):
    my_config = Config(
        url=ARANGO_HOST,
        user=ARANGO_USER,
        password=ARANGO_PASSWORD,
        database=ARANGO_CAG_DB,
        graph=ARANGO_GRAPH
    )
    ELibGraphCreator(files, my_config, initialize=True, load_generic_graph=False)


with Flow(
        "Update elib graph",
        storage=GitLab(repo="35667028", path="elib/elib_cag.py", ref="dev"),
) as flow:
    delta_files = get_delta()
    update_graph(delta_files)

flow.register(project_name="elib", labels=["development"], add_default_labels=False)
