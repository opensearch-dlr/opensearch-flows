import os

import pandas as pd
import prefect
import redis
from cag.framework import GenericAnnotator
from cag.utils.config import Config
from prefect import Flow, task
from prefect.storage import GitLab
from pyArango.connection import *
from sentence_transformers import SentenceTransformer
from tqdm import tqdm

# The redis credentials must be available as environment variables
r = redis.Redis(
    host=str(os.environ.get("REDIS_HOST")),
    port=int(os.environ.get("REDIS_PORT")),
    db=0,
    decode_responses=True,
)
ARANGO_HOST = r.get("ARANGO_HOST")
assert ARANGO_HOST is not None

ARANGO_USER = r.get("ARANGO_USER")
assert ARANGO_USER is not None

ARANGO_PASSWORD = r.get("ARANGO_PASSWORD")
assert ARANGO_PASSWORD is not None

ARANGO_CAG_DB = r.get("ARANGO_CAG_DB")
assert ARANGO_CAG_DB is not None

ARANGO_GRAPH = r.get("ARANGO_GRAPH")
assert ARANGO_GRAPH is not None


class ElibEmbeddingsCreator(GenericAnnotator):
    def __init__(self, config: Config, run=False):
        super().__init__(conf=config, query=None, run=run)
        self.model = SentenceTransformer(
            "sentence-transformers/paraphrase-multilingual-MiniLM-L12-v2")

    def update_graph(self, timestamp=datetime.now(), data=None):
        logger = prefect.context.get("logger")
        # from https://github.com/arangodb/interactive_tutorials/blob/master/notebooks/example_output
        # /WordEmbeddings_output.ipynb
        self.query = """
                    FOR pub in Publication
                        FILTER (!HAS(pub, "title_emb") and pub.title!=null) or (!HAS(pub, "abstract_emb") and pub.abstract!=null)
                        RETURN {_id:pub._id,title:pub.title, abstract:pub.abstract}
                    """
        batch_size = 512
        # note: we do manual paging count, may not be stable!
        total_count = self.query_count()
        if total_count == 0:
            logger.info("No publications found that have no embeddings...")
            return
        logger.info(f"Starting job, total docs: {total_count}")
        ds_collection = self.arango_db["Publication"]

        for i in range(0, total_count, batch_size):
            batch = self.load_page(i, batch_size)
            dataset_batch = list(batch)
            datasets_df_batch = pd.DataFrame(dataset_batch)
            if datasets_df_batch.empty:  # as said, manual batching is sometimes error-prone
                continue
            title_batch = datasets_df_batch.title.tolist()

            abstract_batch: list[str] = datasets_df_batch.abstract.tolist()
            abstract_batch = [
                "" if abstr is None else abstr for abstr in abstract_batch]

            tit_embs = self.model.encode(title_batch)
            ab_embs = self.model.encode(abstract_batch)

            datasets_df_batch.loc[:, "title_emb"] = list(tit_embs)
            datasets_df_batch.loc[:, "abstract_emb"] = list(ab_embs)
            datasets_df_batch["title_emb"] = datasets_df_batch["title_emb"].apply(
                lambda x: x.squeeze().tolist())
            datasets_df_batch["abstract_emb"] = datasets_df_batch["abstract_emb"].apply(
                lambda x: x.squeeze().tolist())
            update_batch = datasets_df_batch.loc[:,
                           ["_id", "title_emb", "abstract_emb"]].to_dict("records")
            ds_collection.update_many(update_batch)


@task(name="Run elib graph annotator")
def run_annotator():
    my_config = Config(
        url=ARANGO_HOST,
        user=ARANGO_USER,
        password=ARANGO_PASSWORD,
        database=ARANGO_CAG_DB,
        graph=ARANGO_GRAPH
    )
    emb_creator = ElibEmbeddingsCreator(my_config)
    emb_creator.update_graph()


with Flow(
        "Run elib graph annotator",
        storage=GitLab(repo="35667028", path="elib/elib_annotator.py", ref="main"),
) as flow:
    run_annotator()

flow.register(project_name="elib", labels=["development"], add_default_labels=False)
