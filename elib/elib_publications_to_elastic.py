import json
import os
import pathlib

import prefect
import redis
from elasticsearch_dsl import Document, Text, Date, Integer, analyzer
from elasticsearch_dsl.connections import connections
from prefect import task, Flow
from prefect.storage import GitLab

# The redis credentials must be available as environment variables
r = redis.Redis(
    host=os.environ.get("REDIS_HOST"),
    port=os.environ.get("REDIS_PORT"),
    db=0,
    decode_responses=True,
)
# In redis there has to be a key with the destination folder as the value
DESTINATION_FOLDER = r.get("ELIB_DESTINATION_FOLDER")
ELASTIC_ADDRESS = r.get("ELASTIC_ADDRESS")

assert all([DESTINATION_FOLDER, ELASTIC_ADDRESS])

connections.create_connection(hosts=[ELASTIC_ADDRESS], timeout=20)

# Analyser used to store text fields
html_strip = analyzer(
    "html_strip",
    tokenizer="standard",
    filter=["lowercase", "stop", "snowball"],
    char_filter=["html_strip"],
)


class Elib(Document):
    """
    Class representing a elastic document
    """

    title = Text(analyzer=html_strip)
    keywords = Text(analyzer=html_strip)
    date = Date()
    eprintid = Integer()
    creators = Text(analyzer=html_strip)

    class Index:
        name = "elib_publications"
        settings = {
            "number_of_shards": 2,
        }


@task(name="Get latest elib ID from destination folder")
def get_highest_elib_id_from_folder():
    """
    Searches recursively the DESTINATION_FOLDER for all files ending with the name *.json and returns the highest
    elib id (specified in the name)
    """
    files = list(pathlib.Path(DESTINATION_FOLDER).glob("*.json"))
    if len(files) == 0:
        latest_id = 1
    else:
        latest_id = max(map(lambda f: int(str(f).split("-")[2].split(".")[0]), files))
    return latest_id


@task(name="Get highest elib ID from elastic")
def get_highest_id_from_elastic():
    """
    Get highest eprintid from elastic using max aggregation
    :return: The highest elib ID from elastic otherwise 0 if there are no docs in elastic.
    """
    logger = prefect.context.get("logger")
    search = Elib.search()
    search = search.params(size=0)
    search.aggs.metric("max_id", "max", field="eprintid")
    result = search.execute()
    if result["aggregations"]["max_id"]["value"] is None:
        logger.warning("Elastic index is empty!")
        return 0
    else:
        max_id = int(result["aggregations"]["max_id"]["value"])
        logger.info(f"Highest ID from elastic: {max_id}")
        return max_id


@task(name="Get diff of IDs")
def get_id_range(latest_id_from_directory, latest_id_from_es):
    """
    Determines the range of elib IDs to create in elastic
    :param latest_id_from_directory:
    :param latest_id_from_es:
    :return:
    """
    logger = prefect.context.get("logger")
    if latest_id_from_directory > latest_id_from_es:
        # desired state, create new documents
        id_range = list(range(latest_id_from_es + 1, latest_id_from_directory + 1))
        logger.info(f"IDs to process: {id_range}")
        return id_range
    elif latest_id_from_directory == latest_id_from_es:
        # everything is up2date, noop
        logger.info(f"ID to process: N/A, everything is up to date")
        return []
    else:
        # something is wrong. elastic has more publications as downloaded
        raise ValueError(
            "The highest id of elastic is larger than the one you can find when searching the elib json dump folder"
        )


@task(name="Create elastic doc from json file")
def create_doc_from_json_file(eprint_id):
    """
    Opens the json of the eprint_id and creates elib document in elastic
    :param eprint_id: The
    """
    logger = prefect.context.get("logger")
    json_file = os.path.join(DESTINATION_FOLDER, f"dlr-eprint-{eprint_id}.json")
    try:
        with open(json_file, "r", encoding="utf8") as f:
            data = json.load(f)  # type: dict
            Elib(
                title=data.get("title", ""),
                keywords=data.get("keywords", ""),
                date=data.get("datestamp"),
                eprintid=data.get("eprintid"),
            ).save()
    except FileNotFoundError:
        logger.info(f"No valid json found for elib ID {eprint_id}")


with Flow(
    "Create elib documents in elastic",
    storage=GitLab(
        repo="35667028", path="elib/elib_publications_to_elastic.py", ref="dev"
    ),
) as flow:
    latest_id_from_directory = get_highest_elib_id_from_folder()
    latest_id_from_es = get_highest_id_from_elastic()
    id_range = get_id_range(latest_id_from_directory, latest_id_from_es)
    create_doc_from_json_file.map(id_range)

flow.register(project_name="elib", labels=["development"], add_default_labels=False)
