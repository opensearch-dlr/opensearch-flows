import json
import os
import pathlib
from datetime import datetime
from json import JSONDecodeError

import prefect
import redis
import requests
from bs4 import BeautifulSoup
from prefect import Flow, task
from prefect.storage import GitLab

# The redis credentials must be available as environment variables
r = redis.Redis(
    host=str(os.environ.get("REDIS_HOST")),
    port=int(os.environ.get("REDIS_PORT")),
    db=0,
    decode_responses=True,
)
# In redis there has to be a key with the destination folder as the value
DESTINATION_FOLDER = r.get("ELIB_DESTINATION_FOLDER")
assert DESTINATION_FOLDER is not None


@task(name="Create destination directory if necessary")
def check_dest_dir():
    """
    Checks if the path in ELIB_DESTINATION_FOLDER is existent
    """
    pathlib.Path(DESTINATION_FOLDER).mkdir(exist_ok=True)


@task(name="Get latest elib ID from website")
def get_latest_eprint_id_from_website():
    """
    Calls the elib page that lists the last publications and determines the current highest elib ID
    """
    url = "https://elib.dlr.de/cgi/latest"
    page = requests.get(url, verify=False)
    soup = BeautifulSoup(page.content, "html.parser")
    divs = soup.find("div", {"class": "ep_latest_result"})
    a = divs.find_next("a")
    latest_id = int(a["href"].split("/")[-2])
    return latest_id


@task(name="Get latest elib ID from destination folder")
def get_highest_elib_id_from_folder():
    """
    Searches recursively the DESTINATION_FOLDER for all files ending with the name *.json and returns the highest
    elib id (specified in the filename, e.g: dlr-eprint-33056.json)
    """
    files = list(pathlib.Path(DESTINATION_FOLDER).glob("*.json"))
    if len(files) == 0:
        latest_id = 1
    else:
        latest_id = max(map(lambda f: int(str(f).split("-")[2].split(".")[0]), files))
    return latest_id


@task(name="Get elib ID range to download")
def get_range_of_ids_to_download(start: int, stop: int):
    """
    Determines from the highest elib id on the website and in the DESTINATION_FOLDER, the range of elib IDs to download
    :param start: Highest elib ID from DESTINATION_FOLDER
    :param stop: Highest (and most up to date) elib ID from elib website
    """
    return range(start, stop + 1)  # range is not inclusive at the end ;)


@task(name="Download elib json to destination")
def download_elib_json(elib_id: int):
    """
    Downloads the json export of an elib publication from the elib site saves it under DESTINATION_FOLDER.
    :param elib_id: The ID of the elib publication
    """
    logger = prefect.context.get("logger")
    url = (
        f"https://elib.dlr.de/cgi/export/eprint/{elib_id}/JSON/dlr-eprint-{elib_id}.js"
    )
    try:
        elib_json = requests.get(url, verify=False)
        elib_json.raise_for_status()
    except requests.exceptions.HTTPError as e:
        logger.info(f"{url} is not a valid URL: {e}")
        return
    # The scheme is dlr-eprint-{elib_id}.json, e.g. dlr-eprint-12345.json for ID 12345
    filename = os.path.join(DESTINATION_FOLDER, f"dlr-eprint-{elib_id}.json")
    try:
        with open(filename, "w") as outfile:
            json.dump(elib_json.json(), outfile)
    except JSONDecodeError:
        # URL serves no valid json -> No elib publication
        logger.info(f"{url} is not a valid elib publication")
        pass


@task(name="Create report")
def report(latest_id_website: int, new_ids: list):
    """
    When this flow is finished, a small report is written to DESTINATION_FOLDER.
    The reports contains when the flow was run, what the latest elib publication from the website was
    and how many new elib publication were found
    :param latest_id_website: latest elib publication from the website
    :param new_ids: List of all processed elib publication IDs
    """
    dest = os.path.join(
        DESTINATION_FOLDER,
        f"report-{datetime.now().strftime('%d_%m_%Y__%H_%M_%S')}.txt",
    )
    with open(dest, "w") as f:
        f.write(f"Report for Run @ {datetime.now().strftime('%d/%m/%Y %H:%M:%S')}\n")
        f.write(
            f"Latest elib publication from website: https://elib.dlr.de/{latest_id_website}/\n"
        )
        f.write(f"Newly downloaded publications:\n")
        for i in new_ids:
            f.write(f"https://elib.dlr.de/{i}/\n")


with Flow(
    "Download elib jsons",
    storage=GitLab(repo="35667028", path="elib/download_elib_jsons.py", ref="dev"),
) as flow:
    check_dest_dir()
    last_eprint_id_from_website = get_latest_eprint_id_from_website()
    highest_id = get_highest_elib_id_from_folder()
    range_to_download = get_range_of_ids_to_download(
        highest_id, last_eprint_id_from_website
    )
    download_elib_json.map(range_to_download)
    report(last_eprint_id_from_website, range_to_download)

flow.register(project_name="elib", labels=["development"], add_default_labels=False)
