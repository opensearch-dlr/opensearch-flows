import glob
import json
import os
from datetime import datetime
from typing import Any

import redis
from bs4 import BeautifulSoup
from cag.framework import GraphCreatorBase
from cag.graph_elements.nodes import GenericOOSNode, Field
from cag.utils import utils
from cag.utils.config import Config
from cag.view_wrapper.arango_analyzer import ArangoAnalyzer
from cag.view_wrapper.link import Link, Field as ViewField
from cag.view_wrapper.view import View
from cag.view_wrapper.arango_analyzer import ArangoAnalyzer, AnalyzerList
from geojson import Point, Polygon, GeometryCollection
from prefect import task, Flow
from prefect.storage import GitLab

# The redis credentials must be available as environment variables
r = redis.Redis(
    host=str(os.environ.get("REDIS_HOST")),
    port=int(os.environ.get("REDIS_PORT")),
    db=0,
    decode_responses=True,
)
PANGEA_CORPUS_DIR = r.get("PANGEA_CORPUS_DIR")
assert PANGEA_CORPUS_DIR is not None

ARANGO_HOST = r.get("ARANGO_HOST")
assert ARANGO_HOST is not None

ARANGO_USER = r.get("ARANGO_USER")
assert ARANGO_USER is not None

ARANGO_PASSWORD = r.get("ARANGO_PASSWORD")
assert ARANGO_PASSWORD is not None

ARANGO_CAG_DB = r.get("ARANGO_CAG_DB")
assert ARANGO_CAG_DB is not None

ARANGO_GRAPH = r.get("ARANGO_GRAPH")
assert ARANGO_GRAPH is not None

ALREADY_PROCESSED = r.get("PANGEA_CAG_ALREADY_PROCESSED")
assert ALREADY_PROCESSED is not None


class Publication(GenericOOSNode):
    _name = "Publication"
    _fields = {
        "display_name": Field(),
        "eprintid": Field(),
        "official_url": Field(),
        "title": Field(),
        "year": Field(),
        "doi": Field(),
        "isbn": Field(),
        "publisher": Field(),
        "geo": Field(),
        "geo_mean": Field(),
        **GenericOOSNode._fields,
    }


class Method(GenericOOSNode):
    _fields = {
        "name": Field(),
        **GenericOOSNode._fields
    }


class Category(GenericOOSNode):
    _fields = {
        "name": Field(),
        **GenericOOSNode._fields
    }


class Person(GenericOOSNode):
    _name = "Person"
    _fields = {
        "display_name": Field(),
        "first_name": Field(),
        "last_name": Field(),
        "email": Field(),
        "orcid_id": Field(),
        **GenericOOSNode._fields,
    }


class PangaeaGraphCreator(GraphCreatorBase):
    _name = "Pangaea"
    _description = "SKG based on the Pangaea Corpus"

    _SKG_EDGE_PUB_METHOD = "PubHasMethod"
    _SKG_EDGE_ANY_CATEGORY = "HasCategory"
    _SKG_EDGE_ANY_DATA = "HasData"
    _SKG_EDGE_CORPUS = "CorpusHas"
    _SKG_EDGE_PUB_ABSTRACT = "PubHasAbstract"
    _SKG_EDGE_ANY_PERSON = "HasPerson"
    _SKG_EDGE_ANY_TERM = "HasKeyterm"

    def convCoverageToGeo(self, coverage):

        geom = None
        coords = coverage['coordinates'] if 'coordinates' in coverage else None
        if 'point' in coverage['type']:
            geom = Point(tuple(coords))

        if 'envelope' in coverage['type']:
            geom = Polygon([[(coords[0][0], coords[0][1]),
                             (coords[1][0], coords[0][1]),
                             (coords[1][0], coords[1][1]),
                             (coords[0][0], coords[1][1]),
                             (coords[0][0], coords[0][1]), ]])

        return geom

    def convCoverageToGeoJSON(self, coverage):
        if 'geometrycollection' in coverage['type']:
            return GeometryCollection([self.convCoverageToGeo(sub_cov) for sub_cov in coverage["geometries"]])
        else:
            return self.convCoverageToGeo(coverage)

    def addTopic(self, topic, conn_vert):
        if type(topic) is str:
            topic = topic
            self.__addSimpleEntry(topic, conn_vert)
        elif topic is not None:
            for t in topic:
                self.__addSimpleEntry(t, conn_vert)

    def addMethd(self, method, conn_vert):
        if type(method) is str:
            self.__addSimpleEntry(method, conn_vert, conn_collection=PangaeaGraphCreator._SKG_EDGE_PUB_METHOD,
                                  simple_collection=Method._name)
        elif method is not None:
            for m in method:
                self.__addSimpleEntry(m, conn_vert, conn_collection=PangaeaGraphCreator._SKG_EDGE_PUB_METHOD,
                                      simple_collection=Method._name)

    def __addSimpleEntry(self, name, conn_vert, conn_collection=None, simple_collection=None):
        if conn_collection is None:
            conn_collection = PangaeaGraphCreator._SKG_EDGE_ANY_CATEGORY
        if simple_collection is None:
            simple_collection = Category._name
        entry_vert = self.upsert_node(
            simple_collection, {'name': name, '_key': utils.encode_name(name)}, alt_key='name')

        return self.upsert_edge(conn_collection, conn_vert, entry_vert, {})

    def __add_pangaea_pub(self, pub_data: dict[str, Any], corpus):
        soup = BeautifulSoup(pub_data['xml'], "xml")
        pub_id = f'pang_{pub_data["_id"]}'
        pub_data['_key'] = pub_id
        pub_data['title'] = soup.citation.title.text
        pub_data['official_url'] = pub_data['URI']
        pub_data['year'] = pub_data['agg-pubYear']
        pub_data['doi'] = pub_data['URI']  # TODO: extract DOI
        pub_data['ISBN'] = None
        pub_data['publisher'] = 'Pangaea'
        pub_data['name'] = soup.citation.title.text
        pub_data['display_name'] = soup.citation.title.text
        terms = pub_data['terms']
        del pub_data['terms']
        del pub_data['_id']
        del pub_data['xml']
        geo = None
        geo_mean = None
        if 'geoCoverage' in pub_data:
            geo = self.convCoverageToGeoJSON(
                pub_data['geoCoverage'])
        if 'meanPosition' in pub_data:
            geo_mean = Point(
                (pub_data['meanPosition']['lon'], pub_data['meanPosition']['lat'],))
        pub_data['geo'] = geo
        pub_data['geo_mean'] = geo_mean

        if soup.abstract is not None:
            pub_data['abstract'] = soup.abstract.text
        else:
            pub_data['abstract'] = None

        pub_vert = self.upsert_node(
            Publication._name, pub_data)
        self.upsert_edge(
            PangaeaGraphCreator._SKG_EDGE_CORPUS, corpus, pub_vert)
        data_doc = {
            '_key': f"data_{pub_id}",
            'name': soup.citation.title.text,
            'url': pub_data['URI']
        }
        data_node = self.upsert_node(
            GraphCreatorBase._DATA_NODE_NAME, data_doc)
        self.upsert_edge(
            PangaeaGraphCreator._SKG_EDGE_ANY_DATA, pub_vert, data_node)
        if soup.abstract is not None:
            text_key = f'text_{pub_id}'
            text_data = {
                'text': soup.abstract.text,
                '_key': text_key
            }
            abstract_key = f'abstract_{pub_id}'
            abstract_data = {
                '_key': abstract_key
            }
            text_vert = self.upsert_node(
                GraphCreatorBase._TEXT_NODE_NAME, text_data)
            abstract_vert = self.upsert_node(
                GraphCreatorBase._ABSTRACT_NODE_NAME, abstract_data)

            self.upsert_edge(
                PangaeaGraphCreator._SKG_EDGE_PUB_ABSTRACT, pub_vert, abstract_vert, {})
            self.upsert_edge(
                GraphCreatorBase._EDGE_ABSTRACT_TEXT, abstract_vert, text_vert, {})
        for author in soup.citation.findAll("author"):
            firstName = author.firstName.text if author.firstName is not None else ''
            lastName = author.lastName.text if author.lastName is not None else ''
            auth_key = utils.encode_name(f'{firstName} {lastName}')
            author_doc = {
                'display_name': f"{firstName} {lastName}",
                'first_name': firstName,
                'last_name': lastName,
                'email': [author.eMail.text] if author.eMail is not None else [],
                'orcid_id': [author.orcid.text] if author.orcid is not None else [],
                '_key': auth_key
            }
            auth_vert = self.upsert_node(
                Person._name, author_doc, alt_key=['first_name', 'last_name'])
            self.upsert_edge(
                PangaeaGraphCreator._SKG_EDGE_ANY_PERSON, pub_vert, auth_vert, {})
        self.__init_term(terms, pub_vert)
        if 'agg-topic' in pub_data:
            self.addTopic(pub_data['agg-topic'], pub_vert)
        if 'agg-method' in pub_data:
            self.addMethd(pub_data['agg-method'], pub_vert)

    def __init_pub_graph(self, corpus):
        if ALREADY_PROCESSED:
            already_processed = set(json.loads(str(ALREADY_PROCESSED)))
        else:
            already_processed = set()
        for publication_file in self.corpus_file_or_dir:
            with open(publication_file, "r") as f:
                try:
                    pub = json.load(f)
                    self.__add_pangaea_pub(pub, corpus)
                except Exception as e:
                    print("Failed pub", publication_file, e)
            already_processed.add(publication_file)
        r.set("PANGEA_CAG_ALREADY_PROCESSED", json.dumps(list(already_processed)))

    def __init_term(self, terms, pub_vert):
        for term_obj in terms:
            del term_obj['_id']
            term_obj['source'] = 'Scraped'
            term_obj['_key'] = utils.encode_name(term_obj['name'])
            term_vert = self.upsert_node(
                GraphCreatorBase._KEY_TERM_NODE_NAME, term_obj, alt_key='name')

            self.upsert_edge(
                PangaeaGraphCreator._SKG_EDGE_ANY_TERM, pub_vert, term_vert, {})
            if "topics" in term_obj:
                self.addTopic(term_obj['topics'], term_vert)

    def init_graph(self):
        analyzer_ngram = ArangoAnalyzer("fuzzy_search_bigram")
        analyzer_ngram.set_edge_ngrams(max=3)
        analyzer_ngram.type = ArangoAnalyzer._TYPE_NGRAM
        analyzer_ngram.set_features()

        analyzer_ngram.create(self.arango_db)

        analyzer_token = ArangoAnalyzer("en_tokenizer")
        analyzer_token.set_stopwords(include_default=False)
        analyzer_token.stemming = True
        analyzer_token.accent = False
        analyzer_token.type = ArangoAnalyzer._TYPE_TEXT
        analyzer_token.set_features()

        analyzer_token.create(self.arango_db)

        # Create Link - a view can hvae 0 to * links
        link = Link(name="Publication")  # Name of a collection in the database
        linkAnalyzers = AnalyzerList(
            ["fuzzy_search_bigram", "en_tokenizer"])
        link.analyzers = linkAnalyzers

        # A link can have 0..* fields
        # text_en is a predifined analyzer from arango
        title_field = Field("title", AnalyzerList(
            ["fuzzy_search_bigram", "en_tokenizer"]))
        abstract_field = Field("abstract", AnalyzerList(
            ["fuzzy_search_bigram", "en_tokenizer"]))
        title_emb_field = Field("title_emb")
        abstract_emb_field = Field("abstract_emb")
        id_field = Field("_id")
        display_field = Field("display_name", AnalyzerList(
            ["fuzzy_search_bigram", "en_tokenizer"]))
        #
        # "title_emb": {},
        # "title": {},
        # "display_name": {},
        # "abstract_emb": {},
        # "abstract": {},
        # "_id": {}
        link.add_field(title_field)
        link.add_field(abstract_field)
        link.add_field(title_emb_field)
        link.add_field(abstract_emb_field)
        link.add_field(id_field)
        link.add_field(display_field)
        # create view
        view = View('publications_view',
                    view_type="arangosearch")
        # add the link (can have 0 or 1 link)
        view.add_link(link)

        # can have 0..* primary sort
        view.add_primary_sort("title", asc=False)
        view.add_stored_value(["title"], compression="lz4")
        try:
            view.create(self.arango_db)
        except Exception as e:
            print("Error creating view, please delete the one on DB?", e)
        corpus = self.create_corpus_node(key="Pangaea", name=PangaeaGraphCreator._name,
                                         type="journal",
                                         desc=PangaeaGraphCreator._description,
                                         created_on=datetime.today())
        self.__init_pub_graph(corpus)


@task(name="Get corpus files to process")
def get_delta():
    paths = set()
    if ALREADY_PROCESSED:
        already_processed = set(json.loads(str(ALREADY_PROCESSED)))
    else:
        already_processed = set()

    for filename in glob.iglob(
            f"{PANGEA_CORPUS_DIR}{os.path.sep}*.json*", recursive=True
    ):
        paths.add(os.path.abspath(filename))
    delta = paths - already_processed
    return list(delta)


@task(name="Run cag")
def update_graph(files):
    my_config = Config(
        url=ARANGO_HOST,
        user=ARANGO_USER,
        password=ARANGO_PASSWORD,
        database=ARANGO_CAG_DB,
        graph=ARANGO_GRAPH
    )
    PangaeaGraphCreator(files, my_config, initialize=True, load_generic_graph=False)


with Flow(
        "Update pangea graph",
        storage=GitLab(repo="35667028", path="pangea/pangea_cag.py", ref="dev"),
) as flow:
    delta_files = get_delta()
    update_graph(delta_files)

flow.register(project_name="pangea", labels=["development"], add_default_labels=False)
