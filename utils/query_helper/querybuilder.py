
from dataclasses import dataclass
from typing import Any, List
from werkzeug.datastructures import MultiDict
from pyArango.connection import *
import requests as rq
import json
from geojson import Feature
import visvalingamwyatt as vw
from sentence_transformers import SentenceTransformer


@dataclass
class SubQuery:
    additional_info: Any = None
    query: str = """FOR ds in Publication
    RETURN {id:ds._id, score:0}"""
    name: str = "base_query"
    execute: bool = True


class Querybuilder:

    def __init__(self, db: Database) -> None:
        self.db = db
        self.embeddings_model = SentenceTransformer(
            "sentence-transformers/paraphrase-multilingual-MiniLM-L12-v2")

    def __build_geoquery(self, place: str) -> SubQuery:
        subquery = SubQuery(name="geo_ids")
        geo_query_rq = rq.get("https://nominatim.openstreetmap.org/search.php", params={
                              'q': place, 'polygon_geojson': '1', 'format': 'jsonv2', 'limit': 1})
        geo_query_resp = geo_query_rq.json()
        if len(geo_query_resp) > 0:
            coords = [float(c) for c in geo_query_resp[0]['boundingbox']]
            bbox = [[(coords[2], coords[0]),
                     (coords[2], coords[1]),
                     (coords[3], coords[1]),
                     (coords[3], coords[0]),
                     (coords[2], coords[0]), ]]

            bbox_str = json.dumps(bbox)
            geojson = geo_query_resp[0]['geojson']
            geojson = vw.simplify_geometry(geojson, threshold=0.1)
            poly_str = json.dumps(geojson)
            subquery.additional_info = Feature(geometry=geo_query_resp[0]['geojson'], properties={'Name': geo_query_resp[0]['display_name'],
                                                                                                  'Type': "Search envelope"})
            subquery.query = f"""
FOR pub IN Publication
    FILTER HAS(pub, 'geo_mean')
    FILTER GEO_INTERSECTS(GEO_POLYGON({bbox_str}), pub.geo_mean)
    FILTER GEO_INTERSECTS({poly_str}, pub.geo_mean)
    RETURN {{id:pub._id, score:0}}
            """
        else:
            subquery.execute = False
            subquery.additional_info = {'error': "Failed to find your geoinfo"}
        return subquery

    def __build_kw_query(self, kws: "list[str]") -> SubQuery:
        subquery = SubQuery(name="kw_query")
        kws = json.dumps(kws)
        subquery.query = f"""
LET kws={kws}
FOR t IN KeyTerm
    FOR t_s IN kws
        FILTER t.name LIKE '%t_s%'
        FOR pub IN 1..1 INBOUND t HasKeyterm
            FILTER IS_SAME_COLLECTION('Publication', pub._id)
            COLLECT id=pub._id
            AGGREGATE score=COUNT(pub._id)
            RETURN {{id, score}}"""
        return subquery

    def __build_persons_query(self, persons: "list[str]") -> SubQuery:
        subquery = SubQuery(name="person_query")
        persons_str = json.dumps(persons)
        subquery.query = f"""
LET persons={persons_str}
FOR p IN Person
    FOR p_s IN persons
        FILTER p.display_name LIKE '%p_s%'
        FOR pub IN 1..1 INBOUND p HasPerson
            FILTER IS_SAME_COLLECTION('Publication', pub._id)
            COLLECT id=pub._id
            AGGREGATE score=COUNT(pub._id)
            RETURN {{id, score}}"""
        return subquery

    def __build_linkage_query(self, term_linkage: "dict[str,Any]") -> SubQuery:
        subquery = SubQuery(name="linkage_ids")

        from_filter = ""
        if term_linkage['from'] is not None:
            from_filter = f"FILTER from_term._id=='{term_linkage['from']['_id']}'"
        link_filter = ""
        if term_linkage['link'] is not None:
            link_filter = f"FILTER link.type=='{term_linkage['link']}'"
        to_filter = ""
        if term_linkage['to'] is not None:
            to_filter = f"FILTER to_term._id=='{term_linkage['to']['_id']}'"
        subquery.query = f"""FOR from_term IN KeyTerm
        {from_filter}
    FOR to_term,link IN 1..1 ANY from_term TermRelation
        {link_filter}
        {to_filter}
        RETURN DISTINCT {{id:link.origin, score:0}}"""
        return subquery

    def __build_fuzzy_search(self, query):
        # TODO: ensure indexes and views

        subquery = SubQuery(name="fuzzy_search_ids")
        subquery.query = f"""
        LET input = "{query}"
LET phraseStructure = (FOR tok IN TOKENS(input, 'text_en')
    RETURN {{
      "LEVENSHTEIN_MATCH": [
        tok,
        3,
        false
      ]
    }})
FOR d IN dataset_view
  SEARCH NGRAM_MATCH(d.name, input, 0.2, 'fuzzy_search_bigram') // matches part of the words to provide context
         OR
         BOOST(PHRASE(d.name, phraseStructure, 'en_tokenizer'), 10) // matches whole words to boost documents containing the matched words
  SORT BM25(d) DESC  
  RETURN {{id:d._id, score: BM25(d) }}
        """
        return subquery

    def __build_fuzzy_search_embeddings(self, query):
        # TODO: ensure indexes and views

        subquery = SubQuery(name="fuzzy_search_emb_ids")
        embeddings = self.embeddings_model.encode(query).tolist()
        subquery.query = f"""
LET descr_emb = (
    {embeddings}
)
LET descr_size = (
  SQRT(SUM(
    FOR i IN RANGE(0, 767)
      RETURN POW(TO_NUMBER(NTH(descr_emb, i)), 2)
  ))
)

FOR v in Publication
    FILTER HAS(v, "title_emb")

    LET v_size = (SQRT(SUM(
      FOR k IN RANGE(0, 767)
        RETURN POW(TO_NUMBER(NTH(v.title_emb, k)), 2)
    )))

    LET numerator = (SUM(
      FOR i in RANGE(0,767)
          RETURN TO_NUMBER(NTH(descr_emb, i)) * TO_NUMBER(NTH(v.title_emb, i))
    ))

    LET cos_sim = (numerator)/(descr_size * v_size)

    RETURN {{id: v._id, score: cos_sim}}

    
        """
        return subquery

    def build_query(self, params: "MultiDict[str, str]", intent=None, full_query=None) -> "tuple[str,dict[str,Any]]":
        # key: created var, value: query
        if intent is None:
            intent = params.get("target", "publications")
        dataset_subqueries: dict[str, SubQuery] = {}
        location_q = params.get("place", default="")
        persons = params.get("persons", default=[])
        keywords = params.get("keywords", default=[])
        term_linkage = json.loads(params.get(
            "term_linkage", default='{"from":null,"link":null,"to":null}'))
        for prop in ['to', 'from', 'link']:
            if prop not in term_linkage.keys():
                term_linkage[prop] = None
        additional_results = {}
        if len(location_q) > 0:
            query = self.__build_geoquery(location_q)
            dataset_subqueries[query.name] = query
        if len(keywords) > 0:
            query = self.__build_kw_query(keywords)
            dataset_subqueries[query.name] = query
        if len(persons) > 0:
            query = self.__build_persons_query(persons)
            dataset_subqueries[query.name] = query
        if term_linkage['from'] is not None or term_linkage['to'] is not None or term_linkage['link'] is not None:
            query = self.__build_linkage_query(term_linkage)
            dataset_subqueries[query.name] = query

        base_query = SubQuery()
        dataset_subqueries[base_query.name] = base_query

        built_query = ""
        for name, query in dataset_subqueries.items():
            query_str = query.query
            built_query += f"LET {name} = ({query_str})\n"
        subqueries_str = ",".join(list(dataset_subqueries.keys()))

        if len(dataset_subqueries) > 1:
            num_subqueries = len(dataset_subqueries)
            built_query += f"""
LET all_ids=UNION({subqueries_str})
LET unique_ids=(FOR aid IN all_ids
    COLLECT id = aid.id
    AGGREGATE score=SUM(aid.score), id_count=COUNT(aid.id)
    FILTER id_count=={num_subqueries}
    RETURN {{id,score}})"""
        else:
            built_query += f"LET unique_ids={subqueries_str}"

        if 'publications' in intent or 'datasets' in intent:
            additional_results['target'] = "publications"
            ids_name = "unique_ids"
            if full_query is not None and len(full_query) > 0:
                embeddings = list(self.embeddings_model.encode(full_query))
                ids_name = "reranked_ids"
                built_query += f"""
                LET descr_emb = ({embeddings})
LET descr_size = (
  SQRT(SUM(
    FOR i IN RANGE(0, 384)
      RETURN POW(TO_NUMBER(NTH(descr_emb, i)), 2)
  ))
)

LET reranked_ids=(FOR uiq IN unique_ids
        LET v=DOCUMENT(uiq.id)
        FILTER HAS(v, "title_emb")
        LIMIT 30
        LET v_size = (SQRT(SUM(
        FOR k IN RANGE(0, 384)
            RETURN POW(TO_NUMBER(NTH(v.title_emb, k)), 2)
        )))

        LET numerator = (SUM(
        FOR i in RANGE(0,384)
            RETURN TO_NUMBER(NTH(descr_emb, i)) * TO_NUMBER(NTH(v.title_emb, i))
        ))

        LET cos_sim = (numerator)/(descr_size * v_size)

        RETURN {{id: v._id, score: cos_sim}})
    """
            built_query += f"""
    FOR uiq IN {ids_name}
        LET pub=DOCUMENT(uiq.id)
        LIMIT 30
        LET proj = (
            for proj in 1..1 OUTBOUND pub PubHasRepo
                return proj
        )
        
        LET pers = (
            for p in 1..1 OUTBOUND pub HasPerson
                return p
        )
        
        LET corp=(FOR c IN 1..1 ANY pub CorpusHas
        return c)[0]
        SORT uiq.score DESC
        RETURN{{pub: UNSET_RECURSIVE(pub,'title_emb','abstract_emb'), proj, pers,score:uiq.score,corp}}
            """
        elif 'persons' in intent:
            additional_results['target'] = "persons"
            # could be improved by 'scoring' authors as they are in random order now
            # e.g. how many publications match here
            built_query += """
LET auth_ids=(FOR pub IN Publication
    FOR uiq IN unique_ids
        FILTER pub._id == uiq.id
        FOR auth IN 1..1  OUTBOUND  pub HasPerson
            COLLECT id = auth._id
            AGGREGATE score=SUM(uiq.score)
            RETURN {score,id})
FOR auth IN Person
    FOR aid IN auth_ids
        FILTER auth._id == aid.id
        SORT aid.score DESC
        LIMIT 30
        RETURN {person:auth, score:aid.score}"""

        for name, query in dataset_subqueries.items():
            if query.additional_info is not None:
                additional_results[name] = query.additional_info
        return built_query, additional_results
