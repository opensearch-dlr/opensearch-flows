from setuptools import setup
setup(name='querybuilder-helpers',
      version='0.1',
      description='Just some helpers',
      author='Benedikt Kantz',
      author_email='benedikt.kantz@dlr.de',
      license='MIT',
      packages=['query_helper'],
      install_requires=['sentence-transformers',
                        'geojson', 'visvalingamwyatt', 'werkzeug'],
      zip_safe=False)
