import json
import os
import pathlib

import pandas as pd
import prefect
import redis
from elasticsearch_dsl import analyzer, Document, Integer, Text, Date
from elasticsearch_dsl.connections import connections
from prefect import task, Flow

# The redis credentials must be available as environment variables
from prefect.storage import GitLab

r = redis.Redis(
    host=os.environ.get("REDIS_HOST"),
    port=os.environ.get("REDIS_PORT"),
    db=0,
    decode_responses=True,
)
# In redis there has to be a key with the destination folder as the value
DESTINATION_FOLDER = r.get("GITLAB_DESTINATION_FOLDER")
ELASTIC_ADDRESS = r.get("ELASTIC_ADDRESS")

assert all([DESTINATION_FOLDER, ELASTIC_ADDRESS])

connections.create_connection(hosts=[ELASTIC_ADDRESS], timeout=20)

# Analyser used to store text fields
html_strip = analyzer(
    "html_strip",
    tokenizer="standard",
    filter=["lowercase", "stop", "snowball"],
    char_filter=["html_strip"],
)


class GitlabProject(Document):
    """
    Class representing a elastic document
    """

    repository_id = Integer()
    name = Text(analyzer=html_strip)
    description = Text(analyzer=html_strip)
    total_commits = Integer()
    created_at = Date()
    last_activity_at = Date()
    web_url = Text(analyzer=html_strip)
    languages = Text(analyzer=html_strip)
    readme = Text(analyzer=html_strip)
    members = Text(analyzer=html_strip)
    contributors = Text(analyzer=html_strip)

    class Index:
        name = "gitlab_project"
        settings = {
            "number_of_shards": 2,
        }


@task(name="Get latest gitlab project ID from destination folder")
def get_highest_project_id_from_folder():
    """
    Searches recursively the DESTINATION_FOLDER for all files ending with the name *.json and returns the highest
    project ID (specified in the filename, e.g: gitlab-project-33056.json)
    """
    files = list(pathlib.Path(DESTINATION_FOLDER).glob("*.json"))
    if len(files) == 0:
        latest_id = None
    else:
        latest_id = max(map(lambda f: int(str(f).split("-")[2].split(".")[0]), files))
    return latest_id


@task(name="Get highest project ID from elastic")
def get_highest_id_from_elastic():
    """
    Get highest project from elastic using max aggregation
    :return: The highest project ID from elastic otherwise 0 if there are no docs in elastic.
    """
    logger = prefect.context.get("logger")
    search = GitlabProject.search()
    search = search.params(size=0)
    search.aggs.metric("max_id", "max", field="repository_id")
    result = search.execute()
    if result["aggregations"]["max_id"]["value"] is None:
        logger.warning("Elastic index is empty!")
        return 0
    else:
        max_id = int(result["aggregations"]["max_id"]["value"])
        logger.info(f"Highest ID from elastic: {max_id}")
        return max_id


@task(name="Get diff of IDs")
def get_id_range(latest_id_from_directory, latest_id_from_es):
    """
    Determines the range of elib IDs to create in elastic
    :param latest_id_from_directory:
    :param latest_id_from_es:
    :return:
    """
    logger = prefect.context.get("logger")
    if latest_id_from_directory > latest_id_from_es:
        # desired state, create new documents
        id_range = list(range(latest_id_from_es + 1, latest_id_from_directory + 1))
        logger.info(f"IDs to process: {id_range}")
        return id_range
    elif latest_id_from_directory == latest_id_from_es:
        # everything is up2date, noop
        logger.info(f"ID to process: N/A, everything is up to date")
        return []
    else:
        # something is wrong. elastic has more publications as downloaded
        raise ValueError(
            "The highest id of elastic is larger than the one you can find when searching the project dump folder"
        )


@task(name="Create elastic doc from json file")
def create_doc_from_json_file(repository_id):
    """
    Opens the json of the repository_id, do some cleaning and saves it to elastic
    :param repository_id: The
    """
    logger = prefect.context.get("logger")
    json_file = os.path.join(DESTINATION_FOLDER, f"gitlab-project-{repository_id}.json")
    try:
        with open(json_file, "r", encoding="utf8") as f:
            data = json.load(f)  # type: dict
            repository_id = data.get("repository_id")
            name = data.get("name")
            description = data.get("description")
            total_commits = data.get("total_commits")
            created_at = data.get("created_at")
            last_activity_at = data.get("last_activity_at")
            web_url = data.get("web_url")
            languages = " ".join(
                data.get("languages").keys()
            ).lower()  # Only take the name, not the percentage
            readme = data.get("readme")
            # members df consists of names in any form (like lastname, firstname, or firstname lastname)
            members = pd.DataFrame({"Full Name": data.get("members")})
            # "lastname, firstname" becomes "firstname lastname"
            members[["Last Name", "First Name"]] = members["Full Name"].str.split(
                ",", expand=True
            )
            # Switch the order from Lastname Firstname to First Name Lastname
            members["Full Name"] = (
                members["First Name"].str.strip()
                + " "
                + members["Last Name"].str.strip()
            )
            # Drop rows with N/A values
            members = members.dropna()
            # using contributors to determine possible email addresses
            # contrib contains name and email
            contrib = pd.DataFrame(data.get("contributors")).drop_duplicates()
            contrib["name"] = contrib["name"].apply(
                lambda name: " ".join(reversed(name.split(",")))
                if "," in name
                else name
            )
            contrib["name"] = contrib["name"].str.strip()
            contrib["email"] = contrib["email"].str.strip()
            contrib = contrib.drop_duplicates()
            # Group by names, because an user may have multiple email addresses
            contrib = contrib.groupby("name").agg(list).reset_index(col_level=0)
            contrib["email"] = " ".join(contrib["email"].to_list()[0])
            GitlabProject(
                repository_id=repository_id,
                name=name,
                description=description,
                total_commits=total_commits,
                created_at=created_at,
                last_activity_at=last_activity_at,
                web_url=web_url,
                languages=languages,
                readme=readme,
                members=" ".join(members["Full Name"].tolist()),
                contributors=" ".join(
                    (contrib["name"] + " " + contrib["email"]).to_list()
                ),
            ).save()

    except FileNotFoundError:
        logger.info(f"No valid json found for repository_id ")


with Flow(
    "Create gitlab project documents in elastic",
    storage=GitLab(repo="35667028", path="gitlab/gitlab_projects_to_es.py", ref="dev"),
) as flow:
    latest_id_from_directory = get_highest_project_id_from_folder()
    latest_id_from_es = get_highest_id_from_elastic()
    id_range = get_id_range(latest_id_from_directory, latest_id_from_es)
    create_doc_from_json_file.map(id_range)

flow.register(project_name="gitlab", labels=["development"], add_default_labels=False)
