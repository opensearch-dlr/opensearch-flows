import glob
import json
import logging
import os
import re
from typing import Union

import bibtexparser
import prefect
import redis
from bibtexparser.bparser import BibTexParser
from cag import logger
from cag.framework import GraphCreatorBase
from cag.graph_elements.nodes import GenericOOSNode, Field
from cag.graph_elements.relations import GenericEdge
from cag.utils import utils
from cag.utils.config import Config
from prefect import task, Flow
from prefect.storage import GitLab
from pyArango.document import Document
from rich.traceback import install
install(show_locals=True)

logger.setLevel(logging.DEBUG)
# The redis credentials must be available as environment variables
r = redis.Redis(
    host=str(os.environ.get("REDIS_HOST")),
    port=int(os.environ.get("REDIS_PORT")),
    db=0,
    decode_responses=True,
)
GITLAB_CORPUS_DIR = r.get("GITLAB_CORPUS_DIR")
assert GITLAB_CORPUS_DIR is not None

ARANGO_HOST = r.get("ARANGO_HOST")
assert ARANGO_HOST is not None

ARANGO_USER = r.get("ARANGO_USER")
assert ARANGO_USER is not None

ARANGO_PASSWORD = r.get("ARANGO_PASSWORD")
assert ARANGO_PASSWORD is not None

ARANGO_CAG_DB = r.get("ARANGO_CAG_DB")
assert ARANGO_CAG_DB is not None

ALREADY_PROCESSED = r.get("GITLAB_CAG_ALREADY_PROCESSED")
assert ALREADY_PROCESSED is not None


class CorpusHas(GenericEdge):
    _fields = GenericEdge._fields


class RepoHasText(GenericEdge):
    _fields = GenericEdge._fields


class RepoHasBibtex(GenericEdge):
    _fields = GenericEdge._fields


class HasMethod(GenericEdge):
    _fields = {
        'timestamp': Field(),
        'code': Field()
    }


class Repository(GenericOOSNode):
    _name = "Repository"
    _fields = {
        'display_name': Field(),
        'repository_id': Field(),
        'name': Field(),
        'description': Field(),
        'total_commits': Field(),
        'created_at': Field(),
        'last_activity_at': Field(),
        'web_url': Field(),
        'languages': Field(),
        'readme': Field(),
        **GenericOOSNode._fields
    }


class Person(GenericOOSNode):
    _name = "Person"
    _fields = {
        "display_name": Field(),
        "first_name": Field(),
        "last_name": Field(),
        "email": Field(),
        "orcid_id": Field(),
        **GenericOOSNode._fields,
    }


class Publication(GenericOOSNode):
    _name = "Publication"
    _fields = {
        "display_name": Field(),
        "eprintid": Field(),
        "official_url": Field(),
        "title": Field(),
        "year": Field(),
        "doi": Field(),
        "isbn": Field(),
        "publisher": Field(),
        "geo": Field(),
        "geo_mean": Field(),
        **GenericOOSNode._fields,
    }


class GitlabGraphCreator(GraphCreatorBase):
    _name = "DLR Gitlab"
    _description = "SKG based on the DLR Gitlab repositories"

    CorpusHas = "CorpusHas"
    HasPerson = "HasPerson"
    RepoHasText = "RepoHasText"
    HasMethod = "HasMethod"
    PubHasRepo = "PubHasRepo"
    RepoHasBibtex = "RepoHasBibtex"

    _edge_definitions = [
        {
            'relation': CorpusHas,
            'from_collections': [GraphCreatorBase._CORPUS_NODE_NAME, ],
            'to_collections': [Repository._name]
        },
        {
            'relation': RepoHasText,
            'from_collections': [Repository._name, ],
            'to_collections': [GraphCreatorBase._EDGE_TEXT_TERM]
        },
        {
            'relation': HasMethod,
            'from_collections': [RepoHasText],
            'to_collections': [GraphCreatorBase._TEXT_NODE_NAME]
        },
        {
            'relation': PubHasRepo,
            'from_collections': [Publication._name],
            'to_collections': [Repository._name]
        },
        {
            'relation': RepoHasBibtex,
            'from_collections': [Repository._name],
            'to_collections': [GraphCreatorBase._TEXT_NODE_NAME]
        },

    ]

    domain_institution_dict = {"dlr.de": "DLR", "google.com": "Google"}

    def init_graph(self):
        pass

    def update(self, files: list):
        if ALREADY_PROCESSED:
            already_processed = set(json.loads(str(ALREADY_PROCESSED)))
        else:
            already_processed = set()
        node_corpus = self.create_corpus_node(key="DLR_gitlab", name=GitlabGraphCreator._name,
                                              type="repositories",
                                              desc=GitlabGraphCreator._description,
                                              created_on=self.now)

        for repo_file in files:
            with open(repo_file, "r") as f:
                project_data = json.load(f)
                # Data for project node
                project = {
                    "_key": f"dlrgit_{project_data['repository_id']}",
                    "repository_id": int(project_data["repository_id"]),
                    "name": project_data["name"],
                    "display_name": project_data["name"],
                    "description": project_data["description"],
                    "total_commits": int(project_data["total_commits"]),
                    "created_at": project_data["created_at"],
                    "last_activity_at": project_data["last_activity_at"],
                    "web_url": project_data["web_url"],
                    "languages": project_data["languages"],
                }
                proj_vert = self.upsert_node(Repository._name, project)
                if proj_vert is not None:
                    # Set corpus
                    self.upsert_edge(
                        GitlabGraphCreator.CorpusHas, node_corpus, proj_vert)
                    # Process contributor nodes
                    for contributor in project_data["contributors"]:
                        first, last = self.clean_up_name(contributor["name"])
                        contrib_doc = {
                            "_key": utils.encode_name(f"{first} {last}"),
                            "first_name": first,
                            "last_name": last,
                            "email": [contributor["email"]],
                            "orcid_id": "",
                            "display_name": contributor["name"]
                        }
                        contrib_vert = self.upsert_node(
                            Person._name, contrib_doc)
                        if contrib_vert is not None:
                            has_contributor = {
                                "project_key": project['_key'],
                                "contributor_key": contrib_doc["_key"],
                            }
                            self.upsert_edge(GitlabGraphCreator.HasPerson,
                                             proj_vert, contrib_vert, has_contributor)
                    # Create text node with readme text and link it to project
                    repo_key = project['_key']
                    text_key = f'text_{repo_key}'
                    text_data = {
                        'text': project_data['readme'],
                        '_key': text_key
                    }
                    text_vert = self.upsert_node(
                        GraphCreatorBase._TEXT_NODE_NAME, text_data)
                    self.upsert_edge(
                        GitlabGraphCreator.RepoHasText, proj_vert, text_vert, {})
                    # Create Method nodes from readmeclassifier
                    if len(project_data["readmeclassifier"]) > 0:
                        for method in project_data["readmeclassifier"]:
                            method_node = self.upsert_node(
                                GraphCreatorBase._TEXT_NODE_NAME, {
                                    "heading": method["heading"],
                                    "content": method["content"]
                                })
                            self.upsert_edge(
                                GitlabGraphCreator.HasMethod, proj_vert, method_node, {
                                    "code": method["code"]
                                })
                    for entry in self.get_bibtex_fields_of_string(project_data['readme']):
                        self.link_repo2elib_via_bibtex(entry, proj_vert)
                        text_vert = self.upsert_node(
                            GraphCreatorBase._TEXT_NODE_NAME, entry)
                        self.upsert_edge(
                            GitlabGraphCreator.RepoHasBibtex, proj_vert, text_vert, {})
            already_processed.add(repo_file)
        r.set("GITLAB_CAG_ALREADY_PROCESSED", json.dumps(list(already_processed)))

    def link_repo2elib_via_bibtex(self, entry: dict, proj_vert: Document):
        cursor = self.database.AQLQuery(f"""
        FOR pub in Publication
            FILTER LEVENSHTEIN_DISTANCE("{entry['title']}", pub.display_name)<5
            RETURN {{_id:pub._id,title:pub.title, abstract:pub.abstract}}
        """)
        for doc in cursor:
            self.upsert_edge(
                GitlabGraphCreator.PubHasRepo, proj_vert, doc, {
                    "bibtex": entry
                })

    def clean_up_name(self, name: str):
        """ returns list [first_name, last_name]
        """
        if "," in name:
            name_split = name.strip().split(",")
            return [name_split[1].strip(), name_split[0].strip()]
        elif " " in name:
            name_split = name.strip().split(" ")
            return [name_split[0].strip(), name_split[-1].strip()]
        else:
            return [name, ""]

    @staticmethod
    def get_bibtex_fields_of_string(text: str) -> Union[list]:
        """

        @param text:

        Returns something like
        [{'ENTRYTYPE': 'article',
        'ID': 'Doe2001',
        'author': 'John Doe',
        'journal': 'Journal of Interesting Papers',
        'pages': '1-10',
        'title': 'An interesting paper',
        'volume': '1',
        'year': '2001'}]
        """
        # regular expression to match bibtex entries
        bibtex_regex = re.compile(r'@[a-zA-Z]+\s*\{.*\}', re.DOTALL)

        # search for the first occurrence of a bibtex entry in the text
        match = bibtex_regex.search(text)

        # check if a match was found
        if match:
            # save the bibtex entry into a variable
            bibtex_entry = match.group()
            # print("Bibtex entry found:", bibtex_entry)
            parser = BibTexParser()
            bib_database = bibtexparser.loads(bibtex_entry, parser=parser)
            if bib_database.entries:
                return bib_database.entries
        return []


@task(name="Get corpus files to process")
def get_delta():
    logger = prefect.context.get("logger")
    paths = set()
    if ALREADY_PROCESSED:
        already_processed = set(json.loads(str(ALREADY_PROCESSED)))
    else:
        already_processed = set()

    for filename in glob.iglob(
            f"{GITLAB_CORPUS_DIR}{os.path.sep}*.json*", recursive=True
    ):
        paths.add(os.path.abspath(filename))
    delta = paths - already_processed
    logger.info(f"Delta files are {delta}")
    return delta


@task(name="Run cag")
def update_graph(files):
    my_config = Config(
        url=ARANGO_HOST,
        user=ARANGO_USER,
        password=ARANGO_PASSWORD,
        database=ARANGO_CAG_DB,
        graph="SKG"
    )
    gc = GitlabGraphCreator(corpus_file_or_dir=files, conf=my_config, initialize=False, load_generic_graph=False)
    gc.update(files)


with Flow(
        "Update gitlab graph",
        storage=GitLab(repo="35667028", path="gitlab/gitlab_cag.py", ref="dev"),
) as flow:
    delta_files = get_delta()
    update_graph(delta_files)

flow.register(project_name="gitlab", labels=["development"], add_default_labels=False)
