import os

import pandas as pd
import prefect
import redis
from cag.framework import GenericAnnotator
from cag.utils.config import Config
from keybert import KeyBERT
from prefect import Flow, task
from prefect.storage import GitLab
from pyArango.connection import *
from sentence_transformers import SentenceTransformer
from tqdm import tqdm

# The redis credentials must be available as environment variables
r = redis.Redis(
    host=str(os.environ.get("REDIS_HOST")),
    port=int(os.environ.get("REDIS_PORT")),
    db=0,
    decode_responses=True,
)
ARANGO_HOST = r.get("ARANGO_HOST")
assert ARANGO_HOST is not None

ARANGO_USER = r.get("ARANGO_USER")
assert ARANGO_USER is not None

ARANGO_PASSWORD = r.get("ARANGO_PASSWORD")
assert ARANGO_PASSWORD is not None

ARANGO_CAG_DB = r.get("ARANGO_CAG_DB")
assert ARANGO_CAG_DB is not None

ARANGO_GRAPH = r.get("ARANGO_GRAPH")
assert ARANGO_GRAPH is not None


class GitlabAnnotator(GenericAnnotator):
    def __init__(self, config: Config, run=False):
        super().__init__(conf=config, query=None, run=run)
        model = SentenceTransformer(
            "sentence-transformers/paraphrase-multilingual-MiniLM-L12-v2")
        self.kw_model = KeyBERT(model=model)

    def update_graph(self, timestamp=datetime.now(), data=None):
        logger = prefect.context.get("logger")
        query = self.database.AQLQuery(f"""
        for repo in Repository
                    FILTER LENGTH(FOR doc IN HasKeyterm FILTER doc._from == repo._id LIMIT 1 RETURN true) == 0
                    FILTER LENGTH(repo.readme) > 0
                    return repo
                """)
        for repo in query:
            logger.info(f"Create keywords for {repo['name']}")
            for keyword in self.kw_model.extract_keywords(repo["readme"]):
                keyterm_node = self.upsert_node("KeyTerm", {
                    "name": keyword[0]
                }, alt_key="name")
                self.upsert_edge(
                    "HasKeyterm",
                    repo,
                    keyterm_node
                )


@task(name="Run gitlab graph annotator")
def run_annotator():
    my_config = Config(
        url=ARANGO_HOST,
        user=ARANGO_USER,
        password=ARANGO_PASSWORD,
        database=ARANGO_CAG_DB,
        graph=ARANGO_GRAPH
    )
    gc = GitlabAnnotator(my_config)
    gc.update_graph()


with Flow(
        "Run gitlab graph annotator",
        storage=GitLab(repo="35667028", path="gitlab/gitlab_annotator.py", ref="dev"),
) as flow:
    run_annotator()

flow.register(project_name="gitlab", labels=["development"], add_default_labels=False)
