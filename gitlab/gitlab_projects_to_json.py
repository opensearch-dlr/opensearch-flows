import base64
import json
import os
import pathlib
import re
from datetime import datetime
from json import JSONDecodeError

import backoff
import gitlab
import markdown
import prefect
import redis
import requests
from bs4 import BeautifulSoup
from cleantext import clean
from gitlab import GitlabGetError, GitlabHttpError, GitlabListError
from prefect import task, Flow
from prefect.storage import GitLab

# The redis credentials must be available as environment variables
r = redis.Redis(
    host=str(os.environ.get("REDIS_HOST")),
    port=int(os.environ.get("REDIS_PORT")),
    db=0,
    decode_responses=True,
)
# In redis there has to be a key with the destination folder as the value
DESTINATION_FOLDER = r.get("GITLAB_DESTINATION_FOLDER")
assert DESTINATION_FOLDER is not None

GITLAB_PRIVATE_TOKEN = r.get("GITLAB_PRIVATE_TOKEN")
assert GITLAB_PRIVATE_TOKEN is not None

GITLAB_URL = r.get("GITLAB_URL")
assert GITLAB_URL is not None

READMECLASSIFIER_URL = r.get("READMECLASSIFIER_URL")
assert READMECLASSIFIER_URL is not None


@task(name="Create destination directory if necessary")
def check_dest_dir():
    """
    Checks if the path in ELIB_DESTINATION_FOLDER is existent
    """
    pathlib.Path(DESTINATION_FOLDER).mkdir(exist_ok=True)


@task(name="Get latest gitlab project ID from destination folder")
def get_highest_project_id_from_folder():
    """
    Searches recursively the DESTINATION_FOLDER for all files ending with the name *.json and returns the highest
    project ID (specified in the filename, e.g: gitlab-project-33056.json)
    """
    files = list(pathlib.Path(DESTINATION_FOLDER).glob("*.json"))
    if len(files) == 0:
        latest_id = None
    else:
        latest_id = max(map(lambda f: int(str(f).split("-")[2].split(".")[0]), files))
    logger = prefect.context.get("logger")
    logger.info(f"Latest Gitlab ID in DESTINATION_FOLDER {latest_id}")
    return latest_id


@task(name="Get project ID range to download")
def get_range_of_ids_to_download(id_after):
    """
    Determines from the highest project id in the DESTINATION_FOLDER, the range of project IDs to download
    :param id_after: In this case, the highest project id found in DESTINATION_FOLDER
    """
    with gitlab.Gitlab(url=GITLAB_URL, private_token=GITLAB_PRIVATE_TOKEN) as gl:
        if id_after:
            return [
                project.id
                for project in gl.projects.list(id_after=id_after, sort="asc", all=True)
            ]
        else:
            return [project.id for project in gl.projects.list(sort="asc", all=True)]


def classify_readme(readme_text: str):
    res = []
    payload = {
        "name": "SomeRepo",
        "content": readme_text
    }
    result = requests.post(READMECLASSIFIER_URL, json=payload)
    if result.status_code != 200:
        return res
    sections = result.json()["response"]
    headers = []
    codes = []
    for idx, entry in enumerate(sections):
        headers.append(entry["heading_markdown"])
        codes.append(entry["section_code"])
    try:
        splitted = re.split("|".join(headers), readme_text)
        if not all(isinstance(item, str) for item in splitted):
            return res
    except re.error:
        return res
    texts = []
    for s in splitted:
        doc = markdown.markdown(s)
        soup = BeautifulSoup(doc, 'lxml')
        texts.append(clean(soup.text))
    for idx, header in enumerate([s.replace("#", '').strip() for s in headers]):
        try:
            content = texts[idx]
        except IndexError:
            content = ""
        res.append(
            {
                "heading": header,
                "code": codes[idx],
                "content": content

            }
        )
    return res


@task(name="Download project information")
def download_gitlab_project_information(project_id: int):
    """
    Downloads the gitlab project information and saves it under DESTINATION_FOLDER
    :param project_id: The ID of the gitlab project
    """
    logger = prefect.context.get("logger")
    logger.info(f"Fetch project {project_id}")
    try:
        with gitlab.Gitlab(url=GITLAB_URL, private_token=GITLAB_PRIVATE_TOKEN) as gl:
            project = gl.projects.get(project_id)
            try:
                readme = base64.b64decode(
                    project.repository_blob(
                        next(
                            item["id"]
                            for item in project.repository_tree()
                            if item["name"].lower() == "README.md".lower()
                        )
                    )["content"]
                ).decode()
                readmeclassifier = classify_readme(readme)
            except StopIteration:
                logger.warning(f"No Readme found for project {project_id}")
                readme = ""
                readmeclassifier = []
            project.name = clean(project.name)
            project_info = {
                "repository_id": project.id,
                "name": project.name,
                "description": project.description,
                "total_commits": len(project.commits.list(all=True)),
                "created_at": project.created_at,
                "last_activity_at": project.last_activity_at,
                "web_url": project.web_url,
                "languages": project.languages(),
                "readme": readme,
                "readmeclassifier": readmeclassifier,
                "members": [
                    clean(member.name) for member in project.members_all.list(all=True)
                ],
                "contributors": [
                    {
                        "name": clean(committer.committer_name),
                        "email": clean(committer.committer_email),
                    }
                    for committer in project.commits.list(all=True)
                ],
            }

            # The scheme is gitlab-project-{project_id}.json, e.g. gitlab-project-33056.json for ID 33056
            filename = os.path.join(
                DESTINATION_FOLDER, f"gitlab-project-{project_id}.json"
            )
            try:
                with open(filename, "w") as outfile:
                    json.dump(project_info, outfile)
            except JSONDecodeError:
                # URL serves no valid json -> No elib publication
                logger.error(f"{project_id} is not a valid gitlab project")
                pass
    except (GitlabGetError, GitlabHttpError, GitlabListError) as e:
        if e.response_code == 404:
            logger.error(f"Error with {project_id} ({e.response_body})")
            return
        if e.response_code == 403:
            logger.error(f"Error with {project_id}: ({e.response_body})")
            return


@task(name="Create report")
def report(new_ids: list):
    """
    When this flow is finished, a small report is written to DESTINATION_FOLDER.
    The reports contains when the flow was run and which new gitlab projects were found and downloaded
    :param new_ids: List of all processed gitlab projectsIDs
    """
    dest = os.path.join(
        DESTINATION_FOLDER,
        f"report-{datetime.now().strftime('%d_%m_%Y__%H_%M_%S')}.txt",
    )
    with open(dest, "w") as f:
        f.write(f"Report for Run @ {datetime.now().strftime('%d/%m/%Y %H:%M:%S')}\n")
        f.write(f"Newly downloaded gitlab projects:\n")
        for i in new_ids:
            f.write(f"{i}\n")


with Flow(
        "Download gitlab project info to json",
        storage=GitLab(
            repo="35667028", path="gitlab/gitlab_projects_to_json.py", ref="dev"
        ),
) as flow:
    check_dest_dir()
    highest_id = get_highest_project_id_from_folder()
    range_to_download = get_range_of_ids_to_download(highest_id)
    download_gitlab_project_information.map(range_to_download)
    report(range_to_download)

flow.register(project_name="gitlab", labels=["development"], add_default_labels=False)
