# Opensearch Flows


ETL tasks are automatically executed with the help of Prefect. There, tasks are logically organized into flows. 
This repository contains various flows created to perform tasks within the Opensearch domain.
It is recommended to read the [Prefect documentation](https://docs-v1.prefect.io/core/getting_started/quick-start.html) 
beforehand.

## Project structure
Three data sources are retrieved as part of Opensearch@DLR:
1. https://elib.dlr.de
2. https://gitlab.dlr.de
3. warcs via DLR internal crawler https://gitlab.com/opensearch-dlr/opensearch-prototype 


Each data source has its own, independent, flows which can be found in the respective folders.

### However, before flows can be sent to the Opensearch prefect, the local prefect client must be configured.
First, prefect must be installed locally, as described 
[here](https://docs.prefect.io/core/getting_started/install.html#basic-installation). 
Then two configuration files must be created or edited. 
These are used by prefect to connect to the central scheduler.

## config.toml
```
# debug mode
debug = true

# base configuration directory
home_dir = "~/.prefect"

backend = "server"

[server]
host = "<address of prefect scheduler>"
port = "4200"
host_port = "4200"
endpoint = "${server.host}:${server.port}"
```

## backend.toml
```
backend = "server"
```
Both files are located under `C:\Users\<USER>\.prefect`