#!/bin/bash
set -e

if [ "$EXTRA_PIP_PACKAGES" ]; then
    echo "EXTRA_PIP_PACKAGES environment variable found. Installing"
    echo $EXTRA_PIP_PACKAGES 
    /usr/local/bin/pip install $EXTRA_PIP_PACKAGES
fi

# Run extra commands
exec "$@"
