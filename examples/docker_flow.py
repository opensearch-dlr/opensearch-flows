import prefect
from prefect import task, Flow
from prefect.storage import Docker


@task(name="print value")
def print_value():
    logger = prefect.context.get("logger")
    logger.warning("Hello world")


with Flow(
    "Docker test",
    storage=Docker(),
) as flow:
    print_value()

flow.register(project_name="testbed", labels=["development"], add_default_labels=False)
