import prefect
from prefect import task, Flow
from prefect.storage import GitLab


@task(name="print value")
def print_value():
    logger = prefect.context.get("logger")
    logger.warning("Hello world")


with Flow(
    "GitLab test",
    storage=GitLab(repo="35667028", path="examples/print_flow.py", ref="dev"),
) as flow:
    print_value()

flow.register(project_name="testbed", labels=["development"], add_default_labels=False)
