# Example Flows

---

Here are some sample flows that cover different aspects of prefect in the scope of
Opensearch. It is recommended to have a look at the [Core Concepts](https://docs.prefect.io/core/concepts/) of prefect beforehand.

## Run a flow
Flows should be deployed via this repository. This means that the agents executing the Flows 
will pull the code _from this repo_. **So make sure, the file is already pushed to this repository!**

Flows are registered in Prefect as follows:
```python
from prefect import Flow
from prefect.storage import GitLab
with Flow(
        "GitLab test",
        storage=GitLab(
            repo="35667028",
            path="examples/print_flow.py",
            ref="dev"),
) as flow:
    # some task
```
The definition of the flow starts with the name of the flow. Here it is `Gitlab test`.
Finally, Prefect must be told where to find the Python file that contains the flow.
The `storage` parameter specifies that the file is on `Gitlab`. Namely in the repository with ID `35667028`
under the path `examples/print_flow.py` in the `dev` branch.

The project ID can be found on top of the main page.

<img src="img/gitlab-project-id.png" width="300" alt=""/>

Please note:

If a repository is _not_ public, the agents must have access to a gitlab access key. 
This key must be set as environment variable `GITLAB_ACCESS_TOKEN`.
## [print_flow.py](print_flow.py)
This is a basic example. It will help you get you started with a simple task which can be executed
from the opensearch prefect.
