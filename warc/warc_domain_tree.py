import contextlib
import glob
import io
import json
import os
import pathlib
from typing import BinaryIO

import redis
from prefect import task, Flow
from prefect.storage import GitLab
from tldextract import tldextract
from warcio import ArchiveIterator, WARCWriter
from warcio.exceptions import ArchiveLoadFailed

# The redis credentials must be available as environment variables
r = redis.Redis(
    host=str(os.environ.get("REDIS_HOST")),
    port=int(os.environ.get("REDIS_PORT")),
    db=0,
    decode_responses=True,
)
WARC_FILES_LOCATION = r.get("WARC_FILES_LOCATION")
assert WARC_FILES_LOCATION is not None
DESTINATION_FOLDER = r.get("WARC_TREE_DESTINATION_FOLDER")
assert DESTINATION_FOLDER is not None
ALREADY_PROCESSED = r.get("WARC_TREE_ALREADY_PROCESSED")
assert ALREADY_PROCESSED is not None


@task(name="Build Domain tree from warc files")
def build_domain_tree():
    """
    Reads recursively warc files under WARC_FILES_LOCATION and builds domain tree
    """
    paths = set()
    if ALREADY_PROCESSED:
        already_processed = set(json.loads(str(ALREADY_PROCESSED)))
    else:
        already_processed = set()

    for filename in glob.iglob(
            f"{WARC_FILES_LOCATION}{os.path.sep}**{os.path.sep}*.warc*", recursive=True
    ):
        paths.add(os.path.abspath(filename))
    tree = {}

    for file in paths - already_processed:  # set operation
        try:
            with open(file, "rb") as stream:  # type: BinaryIO
                try:
                    # ArchiveIterator writes directly to stderr which is bad. We catch those messages and using
                    # the thrown exceptions directly
                    with contextlib.redirect_stderr(io.StringIO()):
                        for record in ArchiveIterator(stream):  # type ArcWarcRecord
                            uri = record.rec_headers.get_header("WARC-Target-URI")
                            if uri:
                                url = tldextract.extract(uri)
                                url_index = f"{url.suffix}/{url.domain}/{url.subdomain}"
                                url_dir = os.path.join(
                                    DESTINATION_FOLDER,
                                    f"{url.suffix}/{url.domain}/{url.subdomain}",
                                )
                                filename = (
                                    f"{url.suffix}.{url.domain}.{url.subdomain}.warc.gz"
                                )
                                if url_index not in tree:
                                    tree[url_index] = {
                                        "writer": io.BytesIO(),
                                        "destination_path": url_dir,
                                        "filename": filename,
                                    }
                                writer = WARCWriter(
                                    tree[url_index]["writer"], gzip=True
                                )
                                writer.write_record(record)

                except (ArchiveLoadFailed, OSError) as e:
                    pass
        except (PermissionError, OSError, FileNotFoundError) as e:
            print(e)
        already_processed.add(file)

    for domain_key, info in tree.items():
        pathlib.Path(info["destination_path"]).mkdir(parents=True, exist_ok=True)
        with open(
                os.path.join(info["destination_path"], info["filename"]), "ab"
        ) as outfile:
            outfile.write(info["writer"].getvalue())
    r.set("WARC_TREE_ALREADY_PROCESSED", json.dumps(list(already_processed)))


with Flow(
        "Build WARC domain tree",
        storage=GitLab(repo="35667028", path="warc/warc_domain_tree.py", ref="dev"),
) as flow:
    build_domain_tree()

flow.register(project_name="warc", labels=["development"], add_default_labels=False)
