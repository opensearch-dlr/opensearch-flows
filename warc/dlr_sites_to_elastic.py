import contextlib
import glob
import io
import json
import os
from typing import BinaryIO

import prefect
import redis
from bs4 import BeautifulSoup
from cleantext import clean
from elasticsearch_dsl import Document, Text, Date, Keyword, analyzer
from elasticsearch_dsl.connections import connections
from prefect import task, Flow
from prefect.storage import GitLab
from warcio import ArchiveIterator
from warcio.exceptions import ArchiveLoadFailed

# The redis credentials must be available as environment variables
r = redis.Redis(
    host=os.environ.get("REDIS_HOST"),
    port=os.environ.get("REDIS_PORT"),
    db=0,
    decode_responses=True,
)
WARC_FILES_LOCATION = r.get("WARC_FILES_LOCATION")
ELASTIC_ADDRESS = r.get("ELASTIC_ADDRESS")
# Check if all variables are NOT none
assert all([WARC_FILES_LOCATION, ELASTIC_ADDRESS])

connections.create_connection(hosts=[ELASTIC_ADDRESS], timeout=20)

# Analyser used to store text fields
html_strip = analyzer(
    "html_strip",
    tokenizer="standard",
    filter=["lowercase", "stop", "snowball"],
    char_filter=["html_strip"],
)


class DLRSite(Document):
    """
    Class representing a elastic document
    """

    url = Keyword()
    indexed_at = Date()
    title = Text(analyzer=html_strip)
    keywords = Text(analyzer=html_strip)
    content = Text(analyzer=html_strip)

    class Index:
        name = "dlr_sites"
        settings = {
            "number_of_shards": 2,
        }


@task(name="Collect already processed warc files and return new ones")
def collect_processed() -> list:
    """
    Reads list from WARC2ELASTIC_ALREADY_PROCESSED and determines if there any new warc files to process
    """
    logger = prefect.context.get("logger")
    paths = set()
    already_processed = r.get("WARC2ELASTIC_ALREADY_PROCESSED")
    if already_processed:
        already_processed = set(json.loads(str(already_processed)))
    else:
        already_processed = set()

    for filename in glob.iglob(
        f"{WARC_FILES_LOCATION}{os.path.sep}**{os.path.sep}*.warc*", recursive=True
    ):
        paths.add(os.path.abspath(filename))
    delta_paths = paths - already_processed
    logger.info(f"New warcs: {delta_paths}")
    return list(delta_paths)


@task(name="Create elastic index from warc files")
def create_elastic_from_warc(file):
    """
    Reads new warc files and saves new websites into elastic
    :param file: Warc file to process
    :return: Returns warc file path
    """
    logger = prefect.context.get("logger")
    logger.warning(f"process {file}")
    try:
        with open(file, "rb") as stream:  # type: BinaryIO
            try:
                # ArchiveIterator writes directly to stderr which is bad. We catch those messages and using
                # the thrown exceptions directly
                with contextlib.redirect_stderr(io.StringIO()):
                    for record in ArchiveIterator(stream):  # type ArcWarcRecord
                        uri = record.rec_headers.get_header("WARC-Target-URI")
                        if uri:
                            soup = BeautifulSoup(record.content_stream().read(), "lxml")
                            texts = [
                                t for t in soup.stripped_strings
                            ]  # type: list[str]
                            if texts[0].startswith("%PDF"):
                                continue
                            full_text = clean(" ".join(texts), no_line_breaks=True)
                            indexed_at = record.rec_headers.get_header("WARC-Date")
                            keywords = soup.find("meta", attrs={"name": "keywords"})
                            if keywords:
                                keywords = clean(
                                    keywords["content"], no_line_breaks=True
                                )
                            else:
                                keywords = " "
                            if soup.find("title"):
                                title = clean(
                                    soup.find("title").string, no_line_breaks=True
                                )
                            else:
                                title = " "
                            DLRSite(
                                url=uri,
                                indexed_at=indexed_at,
                                title=title,
                                keywords=keywords,
                                content=full_text,
                            ).save()

            except (ArchiveLoadFailed, OSError) as e:
                logger.warning(e)
                pass
        logger.info(f"{file} processed")
    except (PermissionError, OSError, FileNotFoundError) as e:
        logger.warning(e)
    finally:
        return file


@task(name="Save which warc files are processed")
def save_processed(paths):
    """
    Saves newly processed warc files into WARC2ELASTIC_ALREADY_PROCESSED
    :param paths: Updated list of all processed warc files as paths
    """
    r.set("WARC_TREE_ALREADY_PROCESSED", json.dumps(list(paths)))


with Flow(
    "DLR Sites to elastic",
    storage=GitLab(repo="35667028", path="warc/dlr_sites_to_elastic.py", ref="dev"),
) as flow:
    DLRSite.init()
    new_warcs = collect_processed()
    warcs_processed = create_elastic_from_warc.map(new_warcs)
    save_processed(warcs_processed)

flow.register(project_name="warc", labels=["development"], add_default_labels=False)
