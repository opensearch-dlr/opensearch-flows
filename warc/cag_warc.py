import glob
import json
import os
from datetime import datetime
from hashlib import sha256

import prefect
import redis
from cag.framework import GraphCreatorBase
from cag.graph_elements.nodes import GenericOOSNode, Field
from cag.graph_elements.relations import GenericEdge
from cag.utils.config import Config
from cag.view_wrapper.arango_analyzer import ArangoAnalyzer, AnalyzerList
from cag.view_wrapper.link import Link, Field as ViewField
from cag.view_wrapper.view import View

from prefect import Flow, task
from prefect.storage import GitLab

r = redis.Redis(
    host=str(os.environ.get("REDIS_HOST")),
    port=int(os.environ.get("REDIS_PORT")),
    db=0,
    decode_responses=True,
)
# In redis there has to be a key with the destination folder as the value
ARANGO_HOST = r.get("ARANGO_HOST")
assert ARANGO_HOST is not None

ARANGO_USER = r.get("ARANGO_USER")
assert ARANGO_USER is not None

ARANGO_PASSWORD = r.get("ARANGO_PASSWORD")
assert ARANGO_PASSWORD is not None

ARANGO_CAG_DB = r.get("ARANGO_CAG_DB")
assert ARANGO_CAG_DB is not None

ARANGO_GRAPH = r.get("ARANGO_GRAPH")
assert ARANGO_GRAPH is not None

CAG_WARC_PROCESSED = r.get("CAG_WARC_PROCESSED")
assert CAG_WARC_PROCESSED is not None

WARC_CORPUS_DIR = r.get("WARC_CORPUS_DIR")
assert WARC_CORPUS_DIR is not None


class DLRSite(GenericOOSNode):
    _name = "DLR_Site"
    _fields = {
        'title': Field(),
        'url': Field(),
        'source': Field(),
        'visited': Field(),
        'title_embedding': Field(),
        'content_embedding': Field(),
        **GenericOOSNode._fields
    }


class CorpusHas(GenericEdge):
    _fields = GenericEdge._fields


class WarcGraphCreator(GraphCreatorBase):
    def init_graph(self):
        pass

    _name = "Warc"
    _description = "SKG based on the Warc Corpus"

    CorpusHas = "CorpusHas"
    DLRSite = "DLRSite"
    _edge_definitions = [
        {
            'relation': CorpusHas,
            'from_collections': [GraphCreatorBase._CORPUS_NODE_NAME],
            'to_collections': [DLRSite]
        },
    ]

    def create_view(self):
        logger = prefect.context.get("logger")
        """
        This method creates Views required for searches from the frontend
        It should be only called (manually) when the View plus Analysers aren't created yet!
        TODO: Automate this task
        """
        analyzer_ngram = ArangoAnalyzer("fuzzy_search_bigram")
        analyzer_ngram.set_edge_ngrams(max=3)
        analyzer_ngram.type = ArangoAnalyzer._TYPE_NGRAM
        analyzer_ngram.set_features()

        analyzer_ngram.create(self.arango_db)

        analyzer_token = ArangoAnalyzer("en_tokenizer")
        analyzer_token.set_stopwords(include_default=False)
        analyzer_token.stemming = True
        analyzer_token.accent = False
        analyzer_token.type = ArangoAnalyzer._TYPE_TEXT
        analyzer_token.set_features()

        analyzer_token.create(self.arango_db)

        # Create Link - a view can hvae 0 to * links
        link = Link(name="DLRSite")  # Name of a collection in the database
        linkAnalyzers = AnalyzerList(
            ["fuzzy_search_bigram", "en_tokenizer"])
        link.analyzers = linkAnalyzers

        # A link can have 0..* fields
        # text_en is a predifined analyzer from arango
        title_field = ViewField("title", AnalyzerList(
            ["fuzzy_search_bigram", "en_tokenizer"]))
        title_emb_field = ViewField("title_emb")
        content_emb_field = ViewField("content_emb")
        id_field = ViewField("_id")

        link.add_field(title_field)
        link.add_field(content_emb_field)
        link.add_field(title_emb_field)
        link.add_field(id_field)
        # create view
        view = View('dlr_sites_view',
                    view_type="arangosearch")
        # add the link (can have 0 or 1 link)
        view.add_link(link)

        # can have 0..* primary sort
        view.add_primary_sort("title", asc=False)
        view.add_stored_value(["title"], compression="lz4")
        try:
            view.create(self.arango_db)
        except Exception as e:
            logger.warning(f"Error creating view, please delete the one on DB? {e}")

    def process_single_warc(self, warc_file: str):
        logger = prefect.context.get("logger")
        logger.info(f"Process {warc_file}")
        with open(warc_file, "r") as f:
            warc_json = json.load(f)
            for entry in warc_json:
                warc_data = {
                    "title": entry["title"],
                    "url": entry["url"],
                    "source": entry["source"],
                    "visited": entry["header"]["WARC-Date"],
                    "title_embedding": entry["title_embedding"],
                    "content_embedding": entry["content_embedding"],
                    "_key": sha256(f"{entry['url']}{entry['header']['WARC-Date']}".encode('utf-8')).hexdigest()
                }

                pub_vert = self.upsert_node("DLRSite", warc_data)
                self.upsert_edge(WarcGraphCreator.CorpusHas, self.corpus, pub_vert)

    def create_corpus(self):
        self.corpus = self.create_corpus_node(key="Warc", name=WarcGraphCreator._name,
                                              type="Website",
                                              desc=self._description,
                                              created_on=datetime.today())


@task(name="Process warc", log_stdout=True)
def process_single(warc_file: str):
    logger = prefect.context.get("logger")
    logger.info(f"Process {warc_file}")
    config = Config(
        url=ARANGO_HOST,
        user=ARANGO_USER,
        password=ARANGO_PASSWORD,
        database=ARANGO_CAG_DB,
        graph=ARANGO_GRAPH
    )
    gc = WarcGraphCreator("", config, initialize=True, load_generic_graph=False)
    gc.create_corpus()
    gc.process_single_warc(warc_file)


@task(name="Create view if needed", log_stdout=True)
def create_view():
    config = Config(
        url=ARANGO_HOST,
        user=ARANGO_USER,
        password=ARANGO_PASSWORD,
        database=ARANGO_CAG_DB,
        graph=ARANGO_GRAPH
    )
    gc = WarcGraphCreator("", config,  initialize=True, load_generic_graph=False)
    gc.create_view()


@task(name="Add newly added warcs to already processed list")
def set_processed(warcs: set):
    if CAG_WARC_PROCESSED:
        already_processed = set(json.loads(str(CAG_WARC_PROCESSED)))
    else:
        already_processed = set()
    r.set("CAG_WARC_PROCESSED", json.dumps(list(already_processed.union(warcs))))


@task(name="Get new warcs")
def get_warc_delta() -> list:
    logger = prefect.context.get("logger")
    paths = set()
    if CAG_WARC_PROCESSED:
        already_processed = set(json.loads(str(CAG_WARC_PROCESSED)))
    else:
        already_processed = set()

    for filename in glob.iglob(
            f"{WARC_CORPUS_DIR}{os.path.sep}*.json*", recursive=True
    ):
        paths.add(os.path.abspath(filename))
    delta = paths - already_processed
    logger.debug(f"New warcs are {delta}")
    return list(delta)


@task(name="Create report")
def report(warcs: set):
    dest = os.path.join(
        os.path.dirname(WARC_CORPUS_DIR),
        f"report-{datetime.now().strftime('%d_%m_%Y__%H_%M_%S')}.txt",
    )
    with open(dest, "w") as f:
        f.write(f"Report for Run @ {datetime.now().strftime('%d/%m/%Y %H:%M:%S')}\n")
        f.write(f"Newly added warcs:\n")
        f.write(', '.join(warcs))


with Flow(
        "Update warcs graph",
        storage=GitLab(repo="35667028", path="warc/cag_warc.py", ref="dev"),
) as flow:
    new_warcs = get_warc_delta()
    process_single.map(new_warcs)
    set_processed(new_warcs)
    create_view()
    report(new_warcs)

flow.register(project_name="warc", labels=["development"], add_default_labels=False)
